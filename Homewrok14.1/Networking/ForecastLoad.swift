//
//  ForecastLoad.swift
//  Homewrok14.1
//
//  Created by Дмитрий Скоробогаты on 08.03.2021.
//

import Foundation
import Alamofire
import RealmSwift

func forecastLoader(completion: @escaping (Result<WeatherForecast, AFError>) -> Void){


    
    AF.request("https://api.openweathermap.org/data/2.5/onecall?lat=55,558741&lon=37,378847&exclude=hourly,current,minutely&units=metric&appid=08880eb824ce844d2f94aa664982e98d")
        .responseDecodable(of: WeatherForecast.self) { (response) in
            guard let forecast = response.value else {
                let error = response.error
                completion(.failure(error!))
                return }
            completion(.success(forecast))
        }
}





