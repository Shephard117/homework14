//
//  WeatherLoad.swift
//  Homewrok14.1
//
//  Created by Дмитрий Скоробогаты on 08.03.2021.
//

import Foundation
import Alamofire


class WeatherLoad{
    
    func loadWeather(completion: @escaping (Result<Weather, AFError>) -> Void){
        
        AF.request("https://api.openweathermap.org/data/2.5/weather?q=Moscow&units=metric&appid=2170806a73a0f226d70ca9a732a74ba3")
        
            .responseDecodable(of: Weather.self) { (response) in
                guard let weather = response.value else {
                    let error = response.error
                    completion(.failure(error!))
                    return }
                completion(.success(weather))
            }
        
    }
}
