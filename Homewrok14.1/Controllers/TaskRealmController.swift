//
//  TaskRealmController.swift
//  Homewrok14.1
//
//  Created by Дмитрий Скоробогаты on 04.03.2021.
//

import UIKit
import RealmSwift

class TaskRealmController: UIViewController {

    var tasks: Results<Tasks>!
    let realm = try! Realm()
    

    @IBOutlet weak var taskTable: UITableView!
    @IBOutlet weak var taskTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tasks = realm.objects(Tasks.self)
        
    }
    
    @IBAction func addTask(_ sender: Any) {
        
        let item = Tasks()
        item.task = taskTextField.text!

        try! realm.write {
            realm.add(item)
        }
        
        taskTextField.text = ""
        
        taskTable.insertRows(at: [IndexPath.init(row: tasks.count - 1 , section: 0)], with: .automatic)
        
    }
}

extension TaskRealmController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = taskTable.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as! TaskRealmCell
        let item = tasks[indexPath.row]
        cell.taskLabel.text = "\(indexPath.row + 1). \(item.task)"
        if item.isComplete == true {
            cell.accessoryType = .checkmark

        } else {
            cell.accessoryType = .none
        }
        
        return cell

    }

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let editingRow = tasks[indexPath.row]
        
        let deleteAction = UIContextualAction(style: .destructive, title: "Удалить") { (_, _, _) in
            
            try! self.realm.write {
                self.realm.delete(editingRow)
                self.taskTable.reloadData()
            }
        }
        let config = UISwipeActionsConfiguration(actions: [deleteAction])
        config.performsFirstActionWithFullSwipe = true
        return config
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        taskTable.deselectRow(at: indexPath, animated: true)
        let item = tasks[indexPath.row]
        try! realm.write {
            item.isComplete = !item.isComplete
            taskTable.reloadData()
        }
    }
    
}

