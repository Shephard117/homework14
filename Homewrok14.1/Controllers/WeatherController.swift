//
//  WeatherController.swift
//  Homewrok14.1
//
//  Created by Дмитрий Скоробогаты on 08.03.2021.
//

import UIKit
import RealmSwift

class WeatherController: UIViewController {

    @IBOutlet weak var forecastTable: UITableView!
    @IBOutlet weak var feelsLike: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var currentTemp: UILabel!
    
    var forecast: WeatherForecast? = nil
    let weatherLoad = WeatherLoad()
    var weather: Weather? = nil
    let realm = try! Realm()
    var forecastRealms: Results<ForecastRealm>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.forecastRealms = realm.objects(ForecastRealm.self)
        locationLabel.text = WeatherModel.shared.name
        currentTemp.text = WeatherModel.shared.currentTemp
        feelsLike.text = WeatherModel.shared.feelsLike
        
        forecastLoader { (result) in
            
            switch result{

            case .success(let forecast):
                self.forecast = forecast
                ForecastRealm.shared.saveForecast(forecast: forecast)
//                print(self.forecastRealm)
                self.forecastTable.reloadData()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    
        weatherLoad.loadWeather { (result) in
            switch result{
            
            case .success(let weather):
                self.weather = weather
                self.locationLabel.text = weather.name
                WeatherModel.shared.name = self.locationLabel.text
                self.currentTemp.text = "\(weather.main.temp)°"
                WeatherModel.shared.currentTemp = self.currentTemp.text
                self.feelsLike.text = "\(weather.main.feels_like)°"
                WeatherModel.shared.feelsLike = self.feelsLike.text
            case .failure(let error):
                print(error)
            }
        }

        
    }
}

extension WeatherController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.forecast?.daily.count ?? 8
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = forecastTable.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as! WeatherCell
        //данные из Realm
        let weatherForecast = Array(forecastRealms)
        if weatherForecast.count != 0{
            for item in weatherForecast{
                let dateRealm = Date(timeIntervalSince1970: TimeInterval(item.dt[indexPath.row]))
                cell.locationLabel.text = item.name
                cell.dateLabel.text = "\(dateRealm)"
                cell.dayLabel.text = "\(item.day[indexPath.row])"
                cell.nightLabel.text = "\(item.night[indexPath.row])"
            }
        }

        //данные из JSon
        if forecast != nil {
            cell.locationLabel.text = forecast?.timezone
            let forecastWeather = forecast?.daily[indexPath.row]
            let date = Date(timeIntervalSince1970: TimeInterval(forecastWeather!.dt))
            cell.dateLabel.text = "\(date)"
            cell.dayLabel.text = "\(forecastWeather?.temp.day ?? 0.0)°"
            cell.nightLabel.text = "\(forecastWeather?.temp.night ?? 0.0)°"
        }
        
        return cell
    }

}
