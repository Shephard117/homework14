//
//  CoreDataController.swift
//  Homewrok14.1
//
//  Created by Дмитрий Скоробогаты on 05.03.2021.
//

import UIKit
import CoreData

class CoreDataController: UIViewController {

    @IBOutlet weak var taskTable: UITableView!
    @IBOutlet weak var taskTextField: UITextField!
    
    var tasks: [ToDoItem] = []
    override func viewDidLoad() {
        super.viewDidLoad()
//        getAllItems()

        // Do any additional setup after loading the view.
    }
    
    private func getContext() -> NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }

    @IBAction func add(_ sender: Any) {
        
        let newTask = taskTextField.text!
        self.saveTask(withTitle: newTask)
        taskTextField.text = ""
    }
    
    private func saveTask(withTitle title: String){
        let context = getContext()
        
        guard let entity = NSEntityDescription.entity(forEntityName: "ToDoItem", in: context) else { return }
        let taskObject = ToDoItem(entity: entity, insertInto: context)
        taskObject.title = title
        
        do {
            try context.save()
            tasks.append(taskObject)
            taskTable.reloadData()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func getAll(){
        let context = getContext()
        let fetchRequest: NSFetchRequest<ToDoItem> = ToDoItem.fetchRequest()

        do {
            tasks = try context.fetch(fetchRequest)
        } catch let error {
            print(error.localizedDescription)
        }
}
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getAll()
    }
}

extension CoreDataController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = taskTable.dequeueReusableCell(withIdentifier: "TaskDataCell", for: indexPath) as! TaskDataCell
        let task = tasks[indexPath.row]
        cell.taskLabel.text = task.title
        
        if task.isDone == true {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let editingRow = tasks[indexPath.row]
        
        let deleteAction = UIContextualAction(style: .destructive, title: "Удалить") { (_, _, _) in
            let context = self.getContext()
            let fetchRequest: NSFetchRequest<ToDoItem> = ToDoItem.fetchRequest()
            if (try? context.fetch(fetchRequest)) != nil{
            context.delete(editingRow)
            }
            
            do {
                try context.save()
                self.getAll()
                self.taskTable.reloadData()
            } catch let error {
                print(error.localizedDescription)
            }
        }
        let config = UISwipeActionsConfiguration(actions: [deleteAction])
        config.performsFirstActionWithFullSwipe = true
        return config
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        taskTable.deselectRow(at: indexPath, animated: true)

        let context = getContext()
        let isDoneObject = tasks[indexPath.row]

        isDoneObject.isDone = !isDoneObject.isDone
        do {
            try context.save()
            taskTable.reloadData()
        } catch let error {
            print(error.localizedDescription)
        }
        

    }
}
