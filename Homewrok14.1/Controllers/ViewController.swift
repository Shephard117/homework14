//
//  ViewController.swift
//  Homewrok14.1
//
//  Created by Дмитрий Скоробогаты on 04.03.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserModel.shared.userName != nil && UserModel.shared.userSurname != nil {
        nameTextField.text = UserModel.shared.userName!
        surnameTextField.text = UserModel.shared.userSurname!
        }
        
    }


    @IBAction func savePerson(_ sender: Any) {
        
        UserModel.shared.userName = nameTextField.text!
        UserModel.shared.userSurname = surnameTextField.text!
        
    }
}

