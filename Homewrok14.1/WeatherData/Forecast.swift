//
//  Forecast.swift
//  Homewrok14.1
//
//  Created by Дмитрий Скоробогаты on 08.03.2021.
//

import Foundation
import RealmSwift


class WeatherForecast: Codable {
    let timezone: String
    let daily: [Daily]
}

class Daily: Codable {
    let dt: Int
    let temp: Temp
}

class Temp: Codable {
    let day, night: Double
}
