//
//  Weather.swift
//  Homewrok14.1
//
//  Created by Дмитрий Скоробогаты on 08.03.2021.
//

import Foundation

struct Main: Codable {
    var temp: Double = 0.0
    var feels_like: Double = 0.0
}
struct Weather: Codable{

    var main: Main = Main()
    var name: String = ""

}
