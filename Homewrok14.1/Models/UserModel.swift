//
//  UserModel.swift
//  Homewrok14.1
//
//  Created by Дмитрий Скоробогаты on 04.03.2021.
//

import Foundation

class UserModel{
    static let shared = UserModel()
    
    private enum Keys: String{
        case userName
        case userSurname
    }
    
    var userName: String?{
        
        set { UserDefaults.standard.setValue(newValue, forKey: Keys.userName.rawValue) }
        get { return UserDefaults.standard.string(forKey: Keys.userName.rawValue) }
        
    }
    var userSurname: String?{
        
        get { return UserDefaults.standard.string(forKey: Keys.userSurname.rawValue) }
        set { UserDefaults.standard.setValue(newValue, forKey: Keys.userSurname.rawValue) }
    }
}
