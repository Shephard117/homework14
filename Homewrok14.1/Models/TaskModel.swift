//
//  TaskModel.swift
//  Homewrok14.1
//
//  Created by Дмитрий Скоробогаты on 04.03.2021.
//

import Foundation
import RealmSwift

class Tasks: Object {
    
    @objc dynamic var task = ""
    @objc dynamic var isComplete: Bool = false

}
