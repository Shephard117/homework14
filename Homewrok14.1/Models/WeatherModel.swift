//
//  WeatherModel.swift
//  Homewrok14.1
//
//  Created by Дмитрий Скоробогаты on 08.03.2021.
//

import Foundation

class WeatherModel{
    static let shared = WeatherModel()
    
    private enum Keys: String{
        case name
        case currentTemp
        case feelsLike
    }
    
    var name: String?{
        
        set { UserDefaults.standard.setValue(newValue, forKey: Keys.name.rawValue) }
        get { return UserDefaults.standard.string(forKey: Keys.name.rawValue) }
        
    }
    
    var currentTemp: String?{
        
        set { UserDefaults.standard.setValue(newValue, forKey: Keys
                                                .currentTemp.rawValue) }
        get { return UserDefaults.standard.string(forKey: Keys
                                                    .currentTemp.rawValue) }
    }
    
    var feelsLike: String?{
        
        set { UserDefaults.standard.setValue(newValue, forKey: Keys.feelsLike.rawValue) }
        get { UserDefaults.standard.string(forKey: Keys.feelsLike.rawValue) }
    }
    
    
}
