//
//  ForecastRealm.swift
//  Homewrok14.1
//
//  Created by Дмитрий Скоробогаты on 09.03.2021.
//

import Foundation
import RealmSwift

class ForecastRealm: Object{
    static let shared = ForecastRealm()
    
    @objc dynamic var name: String = ""
    var dt = List<Int>()
    var day = List<Double>()
    var night = List<Double>()
    
    override static func primaryKey() -> String? {
        "name"
    }
    
    
    func saveForecast(forecast: WeatherForecast?){
        let realm = try! Realm()
        if forecast != nil {
        let forecastRealm = ForecastRealm()
        if forecast != nil {
            forecastRealm.name = forecast!.timezone
            for element in forecast!.daily {

            forecastRealm.dt.append(element.dt)
            forecastRealm.day.append(element.temp.day)
            forecastRealm.night.append(element.temp.night)
            }
        try! realm.write {

            realm.add(forecastRealm, update: .all)
            realm.add(forecastRealm)
            }
            }
        }
    }
}
//    var daily = List<DailyRealm>()
//}
//
//class DailyRealm: Object{
//    @objc dynamic var dt: Int = 0
//    var temp = List<TempRealm>()
//}
//
//class TempRealm: Object{
//
//    @objc dynamic var day: Double = 0.0
//    @objc dynamic var night: Double = 0.0
//
//}

