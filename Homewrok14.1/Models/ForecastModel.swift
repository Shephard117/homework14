//
//  ForecastModel.swift
//  Homewrok14.1
//
//  Created by Дмитрий Скоробогаты on 09.03.2021.
//

import Foundation
import RealmSwift

class ForecastModel: Object {
    @objc dynamic var name: String = ""
    let daily = List<DailyModel>()
    
    override static func primaryKey() -> String? {
          return "name"
        }
}

class DailyModel: Object{
    @objc dynamic var dt: Int = 0
    let temp = List<TempModel>()
    
    override static func primaryKey() -> String? {
          return "dt"
        }
}

class TempModel: Object {
    @objc dynamic var test = ""
    @objc dynamic var day: Int = 0
    @objc dynamic var night: Double = 0.0
 
}
